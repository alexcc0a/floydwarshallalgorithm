package com.nesterov;

public class FloydWarshallAlgorithm {

    public static void main(String[] args) {
        int[][] graph = {{0, 1, 4, Integer.MAX_VALUE}, {Integer.MAX_VALUE, 0, -3, 2},
                {Integer.MAX_VALUE, Integer.MAX_VALUE, 0, -5},
                {Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, 0}}; // Граф в виде матрицы смежности.
        int numVertices = graph.length; // Колличество вершин.

        int[][] distances = floydWarshall(graph, numVertices); // Вызов алгоритма Флойда-Уоршелла.

        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                System.out.println("Расстояние от вершины " + i + " до вершины " + j + " равно " + distances[i][j]);
            }
        }
    }

    public static int[][] floydWarshall(int[][] graph, int numVertices) {
        int[][] distances = new int[numVertices][numVertices];

        // Инициализация расстояний.
        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                distances[i][j] = graph[i][j];
            }
        }

        // Проход по всем вершинам.
        for (int k = 0; k < numVertices; k++) {
            // Проход по всем парам вершин (i, j).
            for (int i = 0; i < numVertices; i++) {
                for (int j = 0; j < numVertices; j++) {
                    // Если путь через вершину k короче текущего пути, то обновляем расстояние.
                    if (distances[i][k] != Integer.MAX_VALUE && distances[k][j] != Integer.MAX_VALUE && distances[i][k] + distances[k][j] < distances[i][j]) {
                        distances[i][j] = distances[i][k] + distances[k][j];
                    }
                }
            }
        }
        return distances;
    }
}
